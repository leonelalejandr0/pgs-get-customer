const colors = require('colors');

module.exports = (req, res, next) => {
    console.info(`${req.method} ${req.originalUrl}`.red);
    next();
}