const express = require('express');
const CustomerCtrl = require('../controllers/Customer.controller');
const Tenant = require('pgs-tenant-middleware');

let app = express();

app.use(require('./middlewares/log-request.middleware'));
app.use(Tenant.validar);

let router = express.Router();

router.get('/get/:identificationNumber/:customerType/:documentType', CustomerCtrl.getCustomer);

app.use(router);


module.exports = app;