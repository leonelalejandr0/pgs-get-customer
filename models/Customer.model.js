const mongoose = require('mongoose');

let getModel = (tenant) =>  {
    const modelName = `Customer_${tenant}`;
    const collectionName = `customer_${tenant}`;
    try {
        let model = mongoose.model(modelName);
        return model;
    } catch (error) {
        let schema = mongoose.Schema({
            _id: String,
            _class : String,
            customerType: String,
            documentType: String,
            identificationNumber: String,
            firstName: String,
            lastName: String,
            email: String,
            phoneNumber1: String,
            phoneNumber2: String
          }, {
              collection: collectionName
            });
      
          let model = mongoose.model(modelName, schema);
          return model;
    }
  };
  
  module.exports = {
      getModel : getModel

  };