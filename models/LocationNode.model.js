const mongoose = require('mongoose');

let getModel = (tenant) =>  {
    const modelName = `LocationNode_${tenant}`;
    const collectionName = `locationNode_${tenant}`;
    try {
        let model = mongoose.model(modelName);
        return model;
    } catch (error) {
        let schema = mongoose.Schema({
            _id: String,
            _class : String,
            name: String,
            normalizationName: String,
            level : Number,
            zipCode: String,
            _countryCode : String,
            _parentNode : String,
            lstSonLocationNode : [this]
          }, {
              collection: collectionName
            });
      
          let model = mongoose.model(modelName, schema);
          return model;
    }
  };
  
  module.exports = {
      getModel : getModel

  };