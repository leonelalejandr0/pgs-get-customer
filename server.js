const express = require('express');
const bodyParser = require('body-parser');
const _database = require('./config/database');
const _routes = require('./routes/routes');
const _config = require('./config/app.config');

const client =  require('cloud-config-client');

const app = express();

const ENVIROMENT = process.env.ENVIROMENT;


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(_config.INITIAL_PATH, _routes);

client.load({
    endpoint: _config.SPRING_CLOUD_CONFIG.ENDPOINT,
    name: _config.SPRING_CLOUD_CONFIG.NAME,
    profiles : [ENVIROMENT] })
  .then(scc => {

      _database.connect(_database.generateConfigDB(scc));

      _config.PORT_SERVER = scc._properties["server.port"];
      app.listen(_config.PORT_SERVER, () => {
        console.log(`El servidor ya está online en el puerto ${_config.PORT_SERVER}`);
      });
    
  }).catch(console.error)

