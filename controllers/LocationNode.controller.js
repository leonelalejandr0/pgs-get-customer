const LocationNodeModel = require('../models/LocationNode.model');

exports.getAll = async (req,res) => {
    if(req.params.regionReference && req.params.cityReference && req.params.communeReference){
        let tenant = req.get('tenant');
        let locationNode = LocationNodeModel.getModel(tenant);
        let location = await getLocation(locationNode, req.params.regionReference, req.params.cityReference, req.params.communeReference);
        let data = {
            region : location[0],
            city : location[1],
            neighbourhood : location[2]
        };
        return res.status(200).json(data);

    }else{
        return res.status(204).json({});
    }
}

let getLocation = async(model, regionReference, cityReference, communeReference) => {
    let arr = [regionReference, cityReference, communeReference];
    let location = await model.find({ _id: { $in : arr } }, '_id name normalizationName _parentNode')
    .sort({ _id : 1})
    .then((results) => {
        return results;
    })
    return location;
};