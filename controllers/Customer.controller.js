const rp = require('request-promise');
const CustomerModel = require('../models/Customer.model');

exports.getCustomer = (req,res) => {
    if(req.params.identificationNumber && req.params.customerType && req.params.documentType){
        let tenant = req.get('tenant');
        let customerModel = CustomerModel.getModel(tenant);
        customerModel.findOne({
            identificationNumber : req.params.identificationNumber,
            customerType : req.params.customerType,
            documentType : req.params.documentType
        }).then(async(customer) => {
            if(customer){
                let c = customer.toJSON();
                

                let location = await getLocation(req.get('tenant'), c.mainAddress);
                c.mainAddress.region = location.region;
                c.mainAddress.city = location.city;
                c.mainAddress.neighbourhood = location.neighbourhood;

                location = await getLocation(req.get('tenant'), c.billingAddress);
                c.billingAddress.region = location.region;
                c.billingAddress.city = location.city;
                c.billingAddress.neighbourhood = location.neighbourhood;

                for (let index = 0; index < c.lstInstallAdresses.length; index++) {
                    const element = c.lstInstallAdresses[index];
                    location = await getLocation(req.get('tenant'), element);
                    
                    
                    element.region = location.region;
                    element.city = location.city;
                    element.neighbourhood = location.neighbourhood;
                    
                }


                
                
                
                
                return res.status(200).json(c);
            }else{
                return res.status(204).json({});
            }
            
        });
        

    }else{
        return res.status(204).json({});
    }
}


let getLocation = async(tenant, address) => {
    let options = {
        uri: `http://localhost:3001/api/location/get/${address.regionReference}/${address.cityReference}/${address.communeReference}`,
        headers: {
          'tenant': tenant
        },
        json : true
      };
      

      let location = await rp(options).then((res) => {
        return res;
      })
      .catch((err) => {
          console.log(err);
      });

      return location;
};