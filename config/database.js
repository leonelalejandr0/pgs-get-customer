const mongoose = require('mongoose');



exports.connect = (db_config) => {

    let uri = `mongodb://${db_config.username}:${db_config.password}@${db_config.host}:${db_config.port}/${db_config.database}?ssl=true&replicaSet=globaldb`;
    
    mongoose
    .connect(uri, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    })
    .then(() =>{
        console.log('MongoDB Conectado!');
    } )
    .catch(err => {
        console.log(err);
    });
}

exports.generateConfigDB = (scc) => {
    return {
        host : scc._properties["spring.data.mongodb.host"],
        database : scc._properties["spring.data.mongodb.database"],
        username : scc._properties["spring.data.mongodb.username"],
        password : scc._properties["spring.data.mongodb.password"],
        port : scc._properties["spring.data.mongodb.port"]
    };
};