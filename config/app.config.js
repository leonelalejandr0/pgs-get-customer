const CONFIG = {
    SPRING_CLOUD_CONFIG : {
       ENDPOINT : "http://localhost:8888",
       NAME : "pgs-ms-customer-node-api"
    },
    INITIAL_PATH : '/api/location',
    PORT_SERVER : 3001
}

module.exports = CONFIG;